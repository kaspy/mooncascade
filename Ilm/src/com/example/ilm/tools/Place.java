package com.example.ilm.tools;

/**
 * Keeps minTemp, maxTemp and place name.
 * 
 * @author Kaspar Peterson
 *
 */
public class Place {
	private String name;
	private String minTemp;
	private String maxTemp;
	
	public Place(String name, String minTemp) {
		this.name = name;
		this.minTemp = minTemp;
	}
	
	public String getName() { return name; }
	
	public String getMinTemp() { return minTemp; }
	
	public String getMaxTemp() { return maxTemp; }
	
	public void setMaxTemp(String maxTemp) { 
		this.maxTemp = maxTemp;
	}
	
}
