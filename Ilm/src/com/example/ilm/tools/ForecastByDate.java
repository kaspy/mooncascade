package com.example.ilm.tools;

/**
 * Object to save Date, min and max temps under allready formated nightTemp and dayTemp.
 * 
 * @author Kaspar Peterson
 *
 */
public class ForecastByDate {

	private String date;
	private String nightPhenomenon;
	private String nightTemp;
	private String dayPhenomenon;
	private String dayTemp;
	
	/**
	 * EMHIParser first finds
	 * 
	 * @param date
	 * @param nighTemp
	 */
	public ForecastByDate(String date, String nightPhenomenon, String nightMin, String nightMax, 
			String dayPhenomenon, String dayMin, String dayMax) {
		String temp[] = date.split("-");
		
		this.date = temp[2] + "." + temp[1];
		this.nightPhenomenon = nightPhenomenon;
		this.nightTemp = nightMin + ".." + nightMax + " °C";
		this.dayPhenomenon = dayPhenomenon;
		this.dayTemp = dayMin + ".." + dayMax + " °C";
	}
	
	public String getDate() { return date; }
	public String getNightTemp() { return nightTemp; }
	public String getDayTemp() { return dayTemp; }
	
}
