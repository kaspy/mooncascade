package com.example.ilm.tools;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import android.util.Log;

import java.io.File;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Parses EMHI's xml.
 * 
 * @author Kaspar Peterson
 *
 */
public class EMHIParser {

	/** If error is null then no error. */
	private String error = "Viga infolugemisel";
	/** Page to read xml from. */
	private String url = "http://www.emhi.ee/ilma_andmed/xml/forecast.php";
	
	private String date;
	
	private String nightPhenomenon;
	private String dayPhenomenon;

	private String nightTempDesc;
	private String nightDesc;

	private String dayTempDesc;
	private String dayDesc;
	
	private String nightMinTemp;
	private String nightMaxTemp;
	private String dayMinTemp;
	private String dayMaxTemp;
	
	private String data;
	
	private LinkedList<Integer> nightMinSpeeds = new LinkedList<Integer>();
	private LinkedList<Integer> nightMaxSpeeds = new LinkedList<Integer>();
	private int nightMinSpeed = Integer.MAX_VALUE;
	private int nightMaxSpeed = Integer.MIN_VALUE;
	
	private LinkedList<Integer> dayMinSpeeds = new LinkedList<Integer>();
	private LinkedList<Integer> dayMaxSpeeds = new LinkedList<Integer>();
	private int dayMinSpeed = Integer.MAX_VALUE;
	private int dayMaxSpeed = Integer.MIN_VALUE;
	
	private LinkedList<ForecastByDate> dates = new LinkedList<ForecastByDate>();
	private LinkedList<Place> places = new LinkedList<Place>();
	
	public String getDate() { return date; }

	public String getNightPhenomenon() { return nightPhenomenon; }
	public String getDayPhenomenon() { return dayPhenomenon; }
	
	public String getNightTemp() { return nightMinTemp + ".." + nightMaxTemp + " °C"; }
	public String getNightTempDesc() { return nightTempDesc; };
	public String getNightDesc() { return nightDesc; }
	
	public String getDayTemp() { return dayMinTemp + ".." + dayMaxTemp + " °C"; }
	public String getDayTempDesc() { return dayTempDesc; };
	public String getDayDesc() { return dayDesc; }
	
	public int getNightMinSpeed() { return nightMinSpeed; }
	public int getNightMaxSpeed() { return nightMaxSpeed; }
	public int getDayMinSpeed() { return dayMinSpeed; }
	public int getDayMaxSpeed() { return dayMaxSpeed; }
	
	public LinkedList<ForecastByDate> getDates() { return dates; }
	public LinkedList<Place> getPlaces() { return places; }
	
	public EMHIParser(String xmlInput) {
		 try {
			
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		    InputSource is = new InputSource();
		    is.setCharacterStream(new StringReader(xmlInput));
			
			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();
		 
			// Adds all forecasts by date to NodeList.
			NodeList forecasts = doc.getElementsByTagName("forecast");
			// Gets first forecast, today's forecast.
			// It is the most detailed and needs some more parsing.
			Node forecast = forecasts.item(0);
	 
			if (forecast.getNodeType() == Node.ELEMENT_NODE) {
	 
				Element eElement = (Element) forecast;
				
				String temp = eElement.getAttribute("date");
				String temps[] = temp.split("-");
				date = temps[2] + "." + temps[1];
				// Finds <night> inside of first <forecast>
				NodeList night = eElement.getElementsByTagName("night");
				Node nNight = night.item(0);
				Element eNight = (Element) nNight;
				
				
				nightPhenomenon = eNight.getElementsByTagName("phenomenon").item(0).getTextContent();
				nightMinTemp =  eNight.getElementsByTagName("tempmin").item(0).getTextContent();
				nightMaxTemp =  eNight.getElementsByTagName("tempmax").item(0).getTextContent();
				nightDesc =  eNight.getElementsByTagName("text").item(0).getTextContent();
				
				// Extracts all speeds.
				for (int i = 0; i < eNight.getElementsByTagName("speedmin").getLength(); i++) {
					nightMinSpeeds.add(Integer.valueOf(eNight.getElementsByTagName("speedmin").item(i).getTextContent()));
					nightMaxSpeeds.add(Integer.valueOf(eNight.getElementsByTagName("speedmax").item(i).getTextContent()));
				}
				
				// Extracts all places from night.	
				NodeList placeNodes = eNight.getElementsByTagName("place");
				
				for (int i = 0; i < placeNodes.getLength(); i++) {
					Node nPlace = placeNodes.item(i);
					Element ePlace = (Element)  nPlace;
					places.add(new Place(ePlace.getElementsByTagName("name").item(0).getTextContent(), 
							ePlace.getElementsByTagName("tempmin").item(0).getTextContent()));
				}
					
				
				// Finds <day> inside of first <forecast>
				NodeList day = eElement.getElementsByTagName("day");
				Node nDay = day.item(0);
				Element eDay = (Element) nDay;
				
				dayPhenomenon = eDay.getElementsByTagName("phenomenon").item(0).getTextContent();
				dayMinTemp = eDay.getElementsByTagName("tempmin").item(0).getTextContent();
				dayMaxTemp = eDay.getElementsByTagName("tempmax").item(0).getTextContent();
				dayDesc =  eDay.getElementsByTagName("text").item(0).getTextContent();
				
				for (int i = 0; i < eDay.getElementsByTagName("speedmin").getLength(); i++) {
					// Speedmin and speedmax come in pairs.
					dayMinSpeeds.add(Integer.valueOf(eDay.getElementsByTagName("speedmin").item(i).getTextContent()));
					dayMaxSpeeds.add(Integer.valueOf(eDay.getElementsByTagName("speedmax").item(i).getTextContent()));
				}
				
				placeNodes = eDay.getElementsByTagName("place");
				
				for (int i = 0; i < placeNodes.getLength(); i++) {
					Node nPlace = placeNodes.item(i);
					Element ePlace = (Element)  nPlace;
					places.get(i).setMaxTemp(ePlace.getElementsByTagName("tempmax").item(0).getTextContent());
				}
				
				
			} 
			
			// Finds as much weathers by date as there is.
			for (int i = 1; i < forecasts.getLength(); i++) {
				String date;
				String nightPhenomenon, nightMin, nightMax;
				String dayPhenomenon, dayMin, dayMax;
				
				forecast = forecasts.item(i);
		 
				if (forecast.getNodeType() == Node.ELEMENT_NODE) {
		 
					Element eElement = (Element) forecast;
					
					date = eElement.getAttribute("date");
					nightPhenomenon = eElement.getElementsByTagName("phenomenon").item(0).getTextContent();
					nightMin = eElement.getElementsByTagName("tempmin").item(0).getTextContent();
					nightMax = eElement.getElementsByTagName("tempmax").item(0).getTextContent();
					
					dayPhenomenon = eElement.getElementsByTagName("phenomenon").item(1).getTextContent();
					dayMin = eElement.getElementsByTagName("tempmin").item(1).getTextContent();
					dayMax = eElement.getElementsByTagName("tempmax").item(1).getTextContent();
					
					dates.add(new ForecastByDate(date, nightPhenomenon, nightMin, nightMax, dayPhenomenon, dayMin, dayMax));
				}
			}
			
	    } catch (Exception e) {
	    	Log.v("EMHI", "error andmete lugemisel");
	    }

		 // Other methods to help format xml.
		 findMinAndMaxSpeeds();
		 getTempDescriptions();
	}
	
	private void getTempDescriptions() {
		IntToWordTranslator translator = new IntToWordTranslator();
		nightTempDesc = translator.getWord(Integer.valueOf(nightMinTemp))
				+ " kuni " + translator.getWord(Integer.valueOf(nightMaxTemp));
		dayTempDesc = translator.getWord(Integer.valueOf(dayMinTemp))
				+ " kuni " + translator.getWord(Integer.valueOf(dayMaxTemp));
		
	}
	
	private void findMinAndMaxSpeeds() {
		Iterator<Integer> iter = nightMinSpeeds.iterator(); 
		
		while(iter.hasNext()) {
			int temp = iter.next();
			if (temp < nightMinSpeed) nightMinSpeed = temp;
		}
		
		iter = nightMaxSpeeds.iterator(); 
		
		while(iter.hasNext()) {
			int temp = iter.next();
			if (temp > nightMaxSpeed) nightMaxSpeed = temp;
		}
		
		iter = dayMinSpeeds.iterator(); 
		
		while(iter.hasNext()) {
			int temp = iter.next();
			if (temp < dayMinSpeed) dayMinSpeed = temp;
		}
		
		iter = dayMaxSpeeds.iterator(); 
		
		while(iter.hasNext()) {
			int temp = iter.next();
			if (temp > dayMaxSpeed) dayMaxSpeed = temp;
		}
	}

}
