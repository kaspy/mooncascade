package com.example.ilm.tools;

/**
 * Class that converts and Integer to Words.
 * For example: 12 -> kaksteist
 * 				 5 -> viis
 * 
 * Highest temperature recorded is 56.7 °C (134 °F) 	
 *    Furnace Creek Ranch (formerly Greenland Ranch), 
 *    Death Valley, California 	1913-07-10
 *    
 * Minimum temperature recorded is −89.2 °C (−128.6 °F)
 *	  Vostok Station 1983-07-21
 *
 * In celsisus 100 is far enough for weather forecast in Estonia.
 * 
 * Solution could be better?
 * 
 * @author Kaspar Peterson
 *
 */
public class IntToWordTranslator {
	
	private String result;
	
	public String getWord(int n) {
		result = "pluss ";
		if (n < 0) {
			n *= -1;
			result = "miinus ";
		} else if (n == 0) return "null kraadi";
		intToWord(n);
		result += " kraadi";
		return result;
	}
	
	public void intToWord(int n) {
		switch (n) {
			case 0: result += "null"; break;
			case 1: result += "üks"; break;
			case 2: result += "kaks"; break;
			case 3: result += "kolm"; break;
			case 4: result += "neli"; break;
			case 5: result += "viis"; break;
			case 6: result += "kuus"; break;
			case 7: result += "seitse"; break;
			case 8: result += "kaheksa"; break;
			case 9: result += "üheksa"; break;
			case 10: result += "kümme"; break;
			case 11: result += "üksteist"; break;
			case 12: result += "kaksteist"; break;
			case 13: result += "kolmteist"; break;
			case 14: result += "neliteist"; break;
			case 15: result += "viisteist"; break;
			case 16: result += "kuusteist"; break;
			case 17: result += "seitseteist"; break;
			case 18: result += "kaheksateist"; break;
			case 19: result += "üheksateist"; break;
			case 20: result += "kakskümmend"; break;
			case 30: result += "kolmkümmend"; break;
			case 40: result += "nelikümmend"; break;
			case 50: result += "viiskümmend"; break;
			case 60: result += "kuuskümmend"; break;
			case 70: result += "seitsekümmend"; break;
			case 80: result += "kaheksakümmend"; break;
			case 90: result += "üheksakümmend"; break; 
			default: other(n); break;
		}
	}
	
	private void other(int n) {
		int decimal = n/10;
		int lastNumber = n%10;
		
		intToWord(decimal*10);
		result += " ";
		intToWord(lastNumber);
		
	}
	
	
}
