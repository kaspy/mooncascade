package com.example.ilm.tools;

/**
 * Chooses right icon for weather.
 * Little bit inaccurate due to small set of icons.
 * Works with all EMHI weather phenomena but does not
 * make difference between night or day.
 * This means that sun is shining in the night :)
 * 
 * @author Kaspar Peterson
 *
 */
public class WeatherIconChooser {
	
	/**
	 * Returns weather icon name that matches phenomena.
	 * @param phenomen
	 */
	public String getIconName(String phenomen) {
		if (phenomen.equals("Clear")) {
			return "sun";
		} else if (phenomen.equals("Few clouds")) {
			return "sun_littlecloud";
		} else if (phenomen.equals("Variable clouds") 
				|| phenomen.equals("Cloudy with clear spells")) {
			return "sun_lightcloud";
		} else if (phenomen.equals("Cloudy")) {
			return "day_lightcloud";
		} else if (phenomen.contains("snow shower")) {
			return "sun_lightcloud_sleet";
		} else if (phenomen.equals("Heavy shower")) {
			return "sun_darkcloud_heavyrain";
		} else if (phenomen.contains("shower")) {
			return "sun_lightcloud_rain";
		} else if (phenomen.equals("Heavy rain")) {
			return "day_darkcloud_heavyrain";
		} else if (phenomen.contains("rain")) {
			return "day_lightcloud_rain";
		} else if (phenomen.contains("sleet")
				|| phenomen.equals("Risk of glaze")) {
			return "day_lightcloud_sleet";
		} else if (phenomen.equals("Hail")
				|| phenomen.equals("Mist")
				|| phenomen.equals("Fog")) {
			return "day_lightcloud";
		} else if(phenomen.contains("Thunder")) {
			return "sun_littlecloud_flash_rain";
		}
		return "sun";
	}
	
	
}
