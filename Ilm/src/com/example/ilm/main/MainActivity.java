package com.example.ilm.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.LinkedList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.example.ilm.R;
import com.example.ilm.tools.EMHIParser;
import com.example.ilm.tools.ForecastByDate;
import com.example.ilm.tools.Place;
import com.example.ilm.tools.WeatherIconChooser;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.os.Build;

/**
 * 
 * @author Kaspar Peterson
 *
 */
public class MainActivity extends ActionBarActivity {

	TextView dateTextView;
	
	TextView nightTempTextView;
	TextView nightTempDescTextView;
	TextView nightDescTextView;
	TextView nightSpeedTextView;
	
	TextView dayTempTextView;
	TextView dayTempDescTextView;
	TextView dayDescTextView;
	TextView daySpeedTextView;
		
	/** Used for printing out progress to user or errors. */
	TextView messageTextView;
	
	TextView placeNightTempTextView;
	TextView placeDayTempTextView;
	
	TextView dateNightTempTextView;
	TextView dateDayTempTextView;
	
	ImageView nightIcon;
	ImageView dayIcon;

	Spinner locationChooser;
	Spinner dateChooser;
	
	EMHIParser parser;
	WeatherIconChooser wiChooser;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		new Parser().execute("");
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
		getThingsReady();
		
	}
	
	/**
	 * Initializes textviews, imageviews and spinners.
	 * Adds spinner onItemSelectedListeners
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 * 
	 */
	private void getThingsReady() {
		
		dateTextView = (TextView) findViewById(R.id.dateTextView);
		
		nightTempTextView = (TextView) findViewById(R.id.nightTempTextView);
		nightTempDescTextView = (TextView) findViewById(R.id.nightTempDescTextView);
		nightDescTextView = (TextView) findViewById(R.id.nightDescTextView);
		nightSpeedTextView = (TextView) findViewById(R.id.nightSpeedTextView);
		
		dayTempTextView = (TextView) findViewById(R.id.dayTempTextView);
		dayTempDescTextView = (TextView) findViewById(R.id.dayTempDescTextView);
		dayDescTextView = (TextView) findViewById(R.id.dayDescTextView);
		daySpeedTextView = (TextView) findViewById(R.id.daySpeedTextView);
		
		messageTextView = (TextView) findViewById(R.id.messageTextView);
		
		placeNightTempTextView = (TextView) findViewById(R.id.placeNightTempTextView);
		placeDayTempTextView = (TextView) findViewById(R.id.placeDayTempTextView);
		
		dateNightTempTextView = (TextView) findViewById(R.id.dateNightTempTextView);
		dateDayTempTextView = (TextView) findViewById(R.id.dateDayTempTextView);
		
		nightIcon = (ImageView) findViewById(R.id.nightIcon);
		dayIcon = (ImageView) findViewById(R.id.dayIcon);
		
		locationChooser = (Spinner) findViewById(R.id.locationChooserSpinner);
		dateChooser = (Spinner) findViewById(R.id.dateChooserSpinner);
		
		wiChooser = new WeatherIconChooser();
		
		
		locationChooser.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				LinkedList<Place> places = parser.getPlaces();
				int counter = 0;
				Iterator<Place> iter = places.iterator(); 
				
				while(iter.hasNext()) {
					Place place = iter.next();
					
					if (counter++ == locationChooser.getSelectedItemPosition()) {
						placeNightTempTextView.setText(place.getMinTemp() + "°C");
						placeDayTempTextView.setText(place.getMaxTemp() + "°C");
					}
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
			
		});
		
		dateChooser.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				LinkedList<ForecastByDate> dates = parser.getDates();
				int counter = 0;
				Iterator<ForecastByDate> iter = dates.iterator(); 
				
				while(iter.hasNext()) {
					ForecastByDate date = iter.next();
					
					if (counter++ == dateChooser.getSelectedItemPosition()) {
						dateNightTempTextView.setText(date.getNightTemp());
						dateDayTempTextView.setText(date.getDayTemp());
					}
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
			
		});

	} 
	
	/**
	 * Changes textviews to data that the parser has extracted.
	 */
	private void updateFields(){
		if (parser != null) {
			dateTextView.setText(parser.getDate());
			nightTempTextView.setText(parser.getNightTemp());
			nightTempDescTextView.setText(parser.getNightTempDesc());
			nightDescTextView.setText(parser.getNightDesc());
			nightSpeedTextView.setText(parser.getNightMinSpeed()
					+ "-" + parser.getNightMaxSpeed() + " m/s");
			
			dayTempTextView.setText(parser.getDayTemp());
			dayTempDescTextView.setText(parser.getDayTempDesc());
			dayDescTextView.setText(parser.getDayDesc());
			daySpeedTextView.setText(parser.getDayMinSpeed()
					+ "-" + parser.getDayMaxSpeed() + " m/s");
			
			messageTextView.setText("");
			
			String img = wiChooser.getIconName(parser.getNightPhenomenon());
			try {
				int id = R.drawable.class.getField(img).getInt(null);
				nightIcon.setImageDrawable(getResources().getDrawable(id));
			} catch (Exception e) {	}
			
			
			img = wiChooser.getIconName(parser.getDayPhenomenon());
			try {
				int id = R.drawable.class.getField(img).getInt(null);
				dayIcon.setImageDrawable(getResources().getDrawable(id));
			} catch (Exception e) { }
			
			setPlaces(0);
			setForecastByDates(0);
			
		} else
			messageTextView.setText("Viga andmete lugemisel");
	}

	/**
	 * Sets spinner values.
	 * 
	 * @param position is the position of chosen item in spinner.
	 */
	private void setPlaces(int position) {
		
		LinkedList<Place> places = parser.getPlaces();
		String array_spinner[] = new String[places.size()];
		int counter = 0;

		Iterator<Place> iter = places.iterator(); 
		
		while(iter.hasNext()) {
			Place place = iter.next();
			
			if (counter == position) {
				placeNightTempTextView.setText(place.getMinTemp() + "°C");
				placeDayTempTextView.setText(place.getMaxTemp() + "°C");
			}
			
			array_spinner[counter++] = place.getName();
		}
		
		ArrayAdapter adapter = new ArrayAdapter(this,
		        android.R.layout.simple_spinner_item, array_spinner);
		        locationChooser.setAdapter(adapter);
		
	}
	/**
	 * Sets spinner values.
	 * 
	 * @param position is the position of chosen item in spinner.
	 */
	private void setForecastByDates(int position) {
		
		LinkedList<ForecastByDate> dates = parser.getDates();
		String array_spinner[] = new String[dates.size()];
		int counter = 0;

		Iterator<ForecastByDate> iter = dates.iterator(); 
		
		while(iter.hasNext()) {
			ForecastByDate date = iter.next();
			
			if (counter == position) {
				dateNightTempTextView.setText(date.getNightTemp());
				dateDayTempTextView.setText(date.getDayTemp());
			}
			
			array_spinner[counter++] = date.getDate();
		}
		
		ArrayAdapter adapter = new ArrayAdapter(this,
		        android.R.layout.simple_spinner_item, array_spinner);
		        dateChooser.setAdapter(adapter);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}
	
	/**
	 * Makes Http connection to EMHI and downloads xml.
	 * Makes new EMHIParser with downloaded xml string.
	 * 
	 * @author Kaspar Peterson
	 *
	 */
	private class Parser extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... arg0) {
			
			try {
				HttpClient client = new DefaultHttpClient();
				HttpGet request = new HttpGet("http://www.emhi.ee/ilma_andmed/xml/forecast.php");
				HttpResponse httpResponse;
				httpResponse = client.execute(request);
				InputStream in = httpResponse.getEntity().getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(in, Charset.forName("iso-8859-1")));
				StringBuilder str = new StringBuilder();
				String line = null;
				while((line = reader.readLine()) != null)
				{
				    str.append(line);
				}
				in.close();

				parser = new EMHIParser(str.toString());
				
				
				
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			updateFields();
		}
		
	}
	

}
